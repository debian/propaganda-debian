propaganda-debian (13.5.11) UNRELEASED; urgency=medium

  * Migrate repository from alioth to salsa.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 27 May 2022 20:38:23 +0100

propaganda-debian (13.5.10) unstable; urgency=low

  * Adopted (closes: #654553)
  * Bump Standards-Version up to 3.9.2
  * Fix lintian warnings.

 -- Adam Michal Ziaja <adam@adamziaja.com>  Tue, 24 Jan 2012 14:41:01 +0100

propaganda-debian (13.5.9) UNRELEASED; urgency=low

  * Lossless-ly optimized all images (jpegoptim -p --strip-all)

 -- Francois Marier <francois@debian.org>  Sat, 08 Jan 2011 17:57:28 +1300

propaganda-debian (13.5.8) unstable; urgency=low

  * Bump Standards-Version up to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 17:01:35 +1300

propaganda-debian (13.5.7) unstable; urgency=medium

  * This is a native package, so switch to 3.0 (native) source format
    so that it can be built from source! (closes: #584346)

 -- Francois Marier <francois@debian.org>  Thu, 03 Jun 2010 22:55:03 +1200

propaganda-debian (13.5.6) unstable; urgency=low

  * Bump Standards-Version to 3.8.4
  * Add a dependency on ${misc:Depends}
  * Switch to 3.0 (quilt) source format

 -- Francois Marier <francois@debian.org>  Thu, 11 Feb 2010 08:38:50 +1300

propaganda-debian (13.5.5) unstable; urgency=low

  * Add git vcs fields to debian/control
  * Bump Standards-Version up to 3.8.3
  * Bump debhelper compatibility to 7
  * Explicitly refer to the local copy of GPL-2

 -- Francois Marier <francois@debian.org>  Mon, 05 Oct 2009 11:11:16 +1300

propaganda-debian (13.5.4) unstable; urgency=low

  * Move build instructions to indep target
  * Bump Standards-Version up to 3.7.3 (no other changes) 

 -- Francois Marier <francois@debian.org>  Mon, 24 Dec 2007 02:56:45 -0500

propaganda-debian (13.5.3) unstable; urgency=low

  * Provide an example XML file for the GNOME wallpaper chooser and talk about 
    it in README.Debian (closes: #416723)

 -- Francois Marier <francois@debian.org>  Fri, 20 Apr 2007 12:17:58 +1200

propaganda-debian (13.5.2) unstable; urgency=low

  * Moved debhelper to Build-Depends
  * Bump Standards-Version to 3.7.2 (no changes)

 -- Francois Marier <francois@debian.org>  Thu, 15 Jun 2006 16:39:25 -0400

propaganda-debian (13.5.1) unstable; urgency=low

  * Update FSF address
  * Bump standards-version up to 3.6.2 (no changes)
  * Drop the chbg suggestion
  * Remove the dash in the version number since this is native package
  * Remove unnecessary lines in debian/rules

 -- Francois Marier <francois@debian.org>  Thu, 19 Jan 2006 00:35:11 -0500

propaganda-debian (13.5-7) unstable; urgency=low

  * Fix lintian warning.
  * Improved description (closes: #303893)

 -- Francois Marier <francois@debian.org>  Sun, 10 Apr 2005 11:46:30 +0200

propaganda-debian (13.5-6) unstable; urgency=low

  * Fixed typo (closes: #222387)
  * Changed maintainer email
  * Bump Standards-version up to 3.6.1 (no changes)

 -- Francois Marier <francois@debian.org>  Mon,  1 Dec 2003 23:13:09 +0100

propaganda-debian (13.5-5) unstable; urgency=low

  * New maintainer (closes: #192656)
  * Rebuilt package using debhelper 4: (closes: #190510)
    - updated Standards-Version to 3.5.10
    - added the names of the authors to the copyright file
    - now also suggests chbg
    - added the original Propaganda README
    - added blurb about chbg and Nautilus in README.Debian
  * Wallpapers are now sitting in /usr/share/wallpapers, just
    like the kdewallpapers package (closes: #185909)
  * HTML files are now in /usr/share/doc/propaganda-debian/html

 -- Francois Marier <francois@debian.org>  Thu, 15 May 2003 22:44:41 -0700

propaganda-debian (13.5-4) unstable; urgency=low

  * Changed url of further propaganda packages to point to x.themes.org

 -- Nicholas Flintham <nick@flinny.demon.co.uk>  Sat,  4 Nov 2000 23:50:38 +0000

propaganda-debian (13.5-3) unstable; urgency=low

  * Fixed typo in README.debian (closes: #62272)

 -- Nicholas Flintham <nick@flinny.demon.co.uk>  Sat, 15 Apr 2000 11:16:53 +0100

propaganda-debian (13.5-2) unstable; urgency=low

  * Now Suggests www-browser
  * Added a more verbose package description (closes: #49908)
  * Moved location of files to /usr/share/backgrounds/propaganda/vol13.5
    to facilitate future additions of more background packages.

 -- Nicholas Flintham <nick@flinny.demon.co.uk>  Tue, 16 Nov 1999 14:40:27 +0000

propaganda-debian (13.5-1) unstable; urgency=low

  * Initial release.

 -- Nicholas Flintham <nick@flinny.demon.co.uk>  Thu,  4 Nov 1999 15:31:01 +0000
